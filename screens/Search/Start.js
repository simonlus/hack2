import React from 'react'
import styled from 'styled-components/native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Alert, AsyncStorage } from 'react-native'
import { fetchUsers } from '../../redux/modules/Match'

import Button from '../../components/Button'

class Start extends React.Component {
  static navigationOptions = {
    header: null
  }

  startSearch = () => {
    if(!this.props.user.activities || this.props.user.activities.length == 0 || 
      !this.props.user.city || this.props.user.city == "" || 
      !this.props.user.bio || this.props.user.bio == "" ) {
        Alert.alert(
          'All profile fields are mandatory',
          'Please fill all fields in the profile section before search',
          [
            {text: 'Go to profile', onPress: async () => {
              this.props.navigation.navigate("ProfileTab");
            } }
          ],
          { cancelable: true }
        );
      } else {
        // this.props.navigation.navigate('SearchingProgress')
        // AsyncStorage.setItem("firstSearch", "true");
        this.props.navigation.navigate('SearchCards')
        // this.props.fetchUsers()
      }
  }

  render () {
    const { navigation, fetchUsers } = this.props

    return (
      <Container>
        <Logo source={require('../../assets/logo.png')} resizeMode="contain" />
        <Button
          text="Start Search"
          onPress={() => this.startSearch()} />
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user.profile
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchUsers
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Start)

const Container = styled.View`
  padding: 16px;
  flex: 1;
  justify-content: center;
  align-items: center;
`

const Logo = styled.Image`
  height: 200px;
  width: 200px;
  margin-top: 24px;
  margin-bottom: 56px;
`

const Title = styled.Text`
  font-size: 24px;
  font-weight: 700;
`
