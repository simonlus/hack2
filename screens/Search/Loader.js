import React from 'react'
import { StatusBar, Animated, Easing } from 'react-native'
import styled from 'styled-components/native'

class Loader extends React.PureComponent {
  state = {
    spinValue: new Animated.Value(0.01),
    scaleValue: new Animated.Value(.5)
  }
  componentWillMount(){
    const seq = Animated.sequence([
      Animated.timing(
        this.state.scaleValue,
        {
          toValue: 1.5,
          duration: 250,
          easing: Easing.linear,
          useNativeDriver: true
        }
      ),
      Animated.loop(
        Animated.timing(
          this.state.spinValue,
          {
            toValue: Math.PI * 2,
            duration: 1000,
            easing: Easing.linear,
            useNativeDriver: true
          }
        )
      ),
    ])
    if(!this.props.noAnimation){
      seq.start();
    }
  }

  render () {
    let { spinValue, scaleValue } = this.state;

    return (
      <Container>
        <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
        <Clickable>
          <Animated.Image 
            style={[this.props.noAnimation ? {} : { transform:[{rotate: spinValue},{scale: scaleValue}]}, {width: 100, height: 100}]}
            source={!this.props.image ? require('../../assets/logo-without-text-inverted.png') : this.props.image } 
            resizeMode="contain" />
          { this.props.noLabel ? null : 
            <LoadingText>
              {this.props.text ? this.props.text : "Chill out while we search for your new best friends"}
            </LoadingText>
          }
        </Clickable>
      </Container>
    )
  }
}

export default Loader

const Container = styled.View`
  flex: 1;
  padding: 0px 16px;
`

const Clickable = styled.View`
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
`

const LoadingText = styled.Text`
  margin-top: 50px;
  max-width: 200px;
  text-align: center;
  color: white;
`