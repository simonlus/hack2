import React from 'react'
import styled from 'styled-components/native'
import { RefreshControl, StatusBar, Dimensions, ScrollView, View } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchUsers, matchAccept, matchDecline } from '../../redux/modules/Match'

import Carousel, { Pagination } from 'react-native-snap-carousel';

import Modal from 'react-native-modal'
import Loader from './Loader'
import Button from '../../components/Button'
import Card from '../../components/Card'
import ReportActions from '../../components/ReportActions'

class Cards extends React.PureComponent {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {}
    return {
      title: null,
      headerRight: <ReportActions user={ params.user } inverted={true}/>,
      headerStyle: {
        backgroundColor: '#5AC6D1',
        shadowColor: 'transparent',
        borderBottomColor: 'transparent',
        elevation: 0,
      },
    }
  }

  state = {
    isReady: false,
    swipedAll: false,
    isModalVisible: true,
    timerDone: false
  }

  componentWillMount() {
    this.props.navigation.addListener('willFocus', async (route) => { 
      const { user, users } = this.props;
      if(!(!user.activities || user.activities.length == 0 || 
        !user.city || user.city == "" || 
        !user.bio || user.bio == "")) {
          await this.props.fetchUsers();
          setTimeout(()=>{
            this.setState({timerDone: true});
          }, 500);
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.matchResult.mutual !== prevProps.matchResult.mutual) {
      this.setState({
        isModalVisible: true
      })
    }
  }
  hideModal = () => this.setState({ isModalVisible: false })

  onSendMessage = () => {
    const {room} = this.props.matchResult.chat
    this.props.navigation.navigate('Chat', { room })
    // this.props.navigation.navigate('Messages')
    this.hideModal()
  }

  _onRefresh = () => {
    this.setState({timerDone: false});
    this.props.fetchUsers();
    setTimeout(()=>{
      this.setState({timerDone: true});
    }, 1000);
  }

  render() {
    const { users, user, usersLoading, matchResult, matchAccept, matchDecline } = this.props
    const { isModalVisible, timerDone } = this.state


    if(!user.activities || user.activities.length == 0 || !user.city || user.city == "" || !user.bio || user.bio == "" ) {
      return <Container>
        <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
        <Title>Well, this is awkward...</Title>
        <GifImage source={require('../../assets/awkward.gif')} />
        <Message>You'll need to finish completing your profile before you can start making new friends 😎</Message>
      </Container>
    } else if(!user.bio || user.bio.length < 80) {
      return <Container>
        <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
        <Title>Well, this is awkward...</Title>
        <GifImage source={require('../../assets/awkward.gif')} />
        <Message>You'll need to finish completing your profile before you can start making new friends 😎 ( Bio length should be greater than 80 characters )</Message>
      </Container>
    }

    if (usersLoading || !timerDone) {
      return <Loader />
    }

    if (users.length == 0 && !usersLoading) {
      return (
        <Container>
          <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
          <Loader />
          {
            matchResult && matchResult.user && matchResult.mutual === true &&
            <Modal isVisible={isModalVisible} onRequestClose={() => this.hideModal()}>
              <ModalView>
                <Title>Good News!</Title>
                <Avatar source={{ uri: matchResult.user.avatarr, cache: 'force-cache' }} />
                <Wants><Name>{matchResult.user.firstname}</Name> wants to go on a Scapade with you!</Wants>
                <ButtonWrapper>
                  <Button text="Send a message" onPress={() => this.onSendMessage()} />
                </ButtonWrapper>
                <Button text="Keep browsing" onPress={() => this.hideModal()} />
              </ModalView>
            </Modal>
          }
        </Container>
      )
    }

    const width = Dimensions.get('window').width;

    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.props.usersLoading}
              onRefresh={this._onRefresh}
            />
          }>
          <Carousel
            ref={c => this._slider1Ref = c}
            contentContainerCustomStyle={{justifyContent: "center", alignItems: "center"}}
            data={users}
            renderItem={({item, index}) => <Card data={item} onAccept={data => matchAccept(data.id)} onDecline={data => matchDecline(data.id)} /> }
            sliderWidth={width}
            itemWidth={width*.8}
            firstItem={0}
            inactiveSlideScale={0.90}
            inactiveSlideOpacity={0.7}
            inactiveSlideShift={40}
            removeClippedSubviews={true}
            initialNumToRender={3}
            maxToRenderPerBatch={3}
            useScrollView={true}
            lockScrollTimeoutDuration={100}
            lockScrollWhileSnapping={true}
            loop={false}
            onLayout={()=>{
              if(this.props.users && this.props.users.length) {
                this.props.navigation.setParams({ user: this.props.users[0]});
                this.setState({ isReady: true })
              }
            }}
            onSnapToItem={(index) => {
              this.setState({ slider1ActiveSlide: index });
              this.props.navigation.setParams({ user: users[index]});
            }}
          />
          {
            matchResult && matchResult.user && matchResult.mutual === true &&
            <Modal isVisible={isModalVisible} onRequestClose={() => this.hideModal()}>
              <ModalView>
                <Title>Good News!</Title>
                <Avatar source={{ uri: matchResult.user.avatar, cache: 'force-cache' }} />
                <Wants><Name>{matchResult.user.firstname}</Name> wants to go on a Scapade with you!</Wants>
                <ButtonWrapper>
                  <Button text="Send a message" onPress={() => this.onSendMessage()} />
                </ButtonWrapper>
                <Button text="Keep browsing" onPress={() => this.hideModal()} />
              </ModalView>
            </Modal>
          }
        </ScrollView>
        { this.props.resultLoading ? <ProgressOverlay><Loader noAnimation={true} noLabel={true} image={
            this.props.resultAction == "accept" ? require("../../assets/check.png") : require("../../assets/reject.png")
          } /></ProgressOverlay> : null }
      </View>
    )
  }
}

const mapStateToProps = state => ({
  users: state.match.users,
  user: state.user.profile,
  usersLoading: state.match.usersLoading,
  matchResult: state.match.matchResult,
  resultLoading: state.match.loading,
  resultAction: state.match.action,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchUsers,
      matchAccept,
      matchDecline
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Cards)

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 16px 16px;
`

const NoUsers = styled.Text`
  font-size: 16px;
`

const ModalView = styled.View`
  background: #FFF;
  border-radius: 8px;
  padding: 32px 16px;

  justify-content: center;
  align-items: center;
`

const Title = styled.Text`
  color: #FFF;
  font-size: 32px;
  font-weight: 600;
  text-align: center;
`

const Message = styled.Text`
  color: #FFF;
  font-size: 16px;
  text-align: center;
  margin: 40px;
  margin-top: 0px;
`

const Avatar = styled.Image`
  width: 100px;
  height: 100px;
  margin-bottom: 16px;
  border-radius: 50px;
`

const Name = styled.Text`
  font-weight: 600;
`

const Wants = styled.Text`
  font-size: 16px;
  text-align: center;
  width: 80%;
  margin-bottom: 40px;
`

const ButtonWrapper = styled.View`
  margin-bottom: 16px;
  width: 100%;
`
const ProgressOverlay = styled.View`
  position: absolute;
  background-color: rgba(255,255,255,.9);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
`
const GifImage = styled.Image`
  resize-mode: contain;
  height: 140px;
  margin: 10px;
`
