import React from 'react'
import styled from 'styled-components/native'
import { StatusBar } from 'react-native'

export default class Explore extends React.PureComponent {
    render() {
        return (
            <Container>
                <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
                <Title>Shhh! Coming soon</Title>
                <GifImage source={require('../../assets/gather.gif')} />
                <Message>Activity inspiration and special offers based on your interests</Message>
            </Container>
        )
    }
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 16px 16px;
`
const Title = styled.Text`
  color: #FFF;
  font-size: 32px;
  font-weight: 600;
  text-align: center;
`
const Message = styled.Text`
  color: #FFF;
  font-size: 16px;
  text-align: center;
  margin: 40px;
  margin-top: 0px;
`

const GifImage = styled.Image`
  resize-mode: contain;
  height: 140px;
  margin: 10px;
`