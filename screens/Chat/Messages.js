import React from 'react'
import styled from 'styled-components/native'
import _ from 'lodash'
import { ActivityIndicator, RefreshControl, ListView, View, StatusBar } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { loadConversations } from '../../redux/modules/Chat/ChatActions'
// import { }from '@patwoz/react-navigation-is-focused-hoc'

import ChatActivity from '../../components/ChatActivity'

function cutMessage (message) {
  return message.length > 40 ? message.split('').slice(0, 37).join('') + '...' : message
}

class Messages extends React.Component {
  static navigationOptions = {
    title: 'Conversations',
    headerTintColor: '#5AC6D1',
    headerStyle: {
      shadowColor: 'transparent',
      borderBottomColor: 'transparent',
      elevation: 0,
    },
  }

  ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

  componentWillMount(){
    this.props.loadConversations();
  }

  renderRow = ({user, lastMessage, room, unread}, index) => {
    return <Dialog onPress={() => this.handleDialogPress(room)} key={index}>
      <Photo source={{ uri: user.avatar }} resizeMode="cover" />
      <Information>
        <Header>
          <Name>{user.firstname}</Name>
          <Activities>
            {
              user.activities.map((activity, index) => (
                <ChatActivity icon={activity} key={index} />
              ))
            }
          </Activities>
        </Header>
        <Message>{cutMessage(lastMessage.text || '' )}</Message>
      </Information>
      { unread != 0 ? <Badge><BadgeText>{unread}</BadgeText></Badge> : null }
    </Dialog>
  }

  handleDialogPress (room) {
    const { navigation: { navigate } } = this.props
    navigate('Chat', { room });
  }

  render () {
    let { loading, conversations } = this.props.chat;

    if (loading) {
      return (
        <Preloader>
          <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
          <ActivityIndicator size="large" color="#5AC6D1" />
        </Preloader>
      );
    }

    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
        <List
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={this.props.loadConversations}
            />
          }
          dataSource={this.ds.cloneWithRows(conversations)}
          renderRow={this.renderRow}
        />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  chat: state.chat,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadConversations
    },
    dispatch
  )

const areStatesEqual = (next, prev) => {
  _.isEqual(next, prev)
}

export default connect(mapStateToProps, mapDispatchToProps, null, {pure: false, areStatesEqual })(Messages)

const Preloader = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
`

const List = styled.ListView`
  padding-top: 24px;
  flex: 1;
`

const Dialog = styled.TouchableOpacity`
  padding: 8px 16px;
  flex-flow: row nowrap;
  align-items: center;
  margin-bottom: 4px;
`

const Photo = styled.Image`
  height: 56px;
  width: 56px;
  border-radius: 28px;
`

const Information = styled.View`
  margin-left: 16px;
  flex: 1;
`

const Header = styled.View`
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;
`

const Name = styled.Text`
  font-weight: 600;
  color: #4A4A4A;
  font-size: 16px;
`

const Message = styled.Text`
  color: #9B9B9B;
  font-size: 14px;
`

const Activities = styled.View`
  flex: 1;
  flex-flow: row nowrap;
  justify-content: flex-end;
  align-items: center;
`

const Badge = styled.View`
  position: absolute;
  background-color: red;
  border-radius: 10px;
  border: 2px;
  border-color: #FFF;
  height: 20px;
  width: 20px;
  left: 55px;
  bottom: 10px;
`

const BadgeText = styled.Text`
  color: #FFF;
  font-size: 12;
  text-align: center;
  line-height: 16px;
`