import React from 'react'
import { Keyboard, Platform, View, ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
// import { sendMessage } from '../../redux/modules/Chat';
import { loadRoom, leaveRoom, sendMessage } from '../../redux/modules/Chat/ChatActions'
import _ from 'lodash'
import { GiftedChat } from 'react-native-gifted-chat'
//components
import ReportActions from '../../components/ReportActions'

class Chat extends React.PureComponent {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {}
    return {
      // title: params.user ? params.user.fullname : '',
      headerStyle: {
        shadowColor: 'transparent',
        borderBottomColor: 'transparent',
        elevation: 0,
      },
      headerTintColor: '#5AC6D1',
      headerRight: <ReportActions user={ params.user } navigation={navigation} />
    }
  }

  componentDidMount() {
    const { navigation: { state: { params: {room} } } } = this.props
    if(Platform.OS === 'android') {
      this.props.navigation.setParams({ tabBarVisible: false })
    }
    this.props.loadRoom(room)
  }

  componentWillUnmount() {
    const { navigation: { state: { params: {room} } } } = this.props
    this.props.leaveRoom(room)
    this.props.navigation.setParams({ user: null, tabBarVisible: true }) 
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.chat.currentRoom && nextProps.chat.currentRoom) {
      const { user } = nextProps.chat.currentRoom.members.find(m=>this.props.profile.id!=m.user.id)
      this.props.navigation.setParams({ user: user }) 
    }
  }

  render () {
    const { chat: { currentRoom, loadingRoom}, profile : { _id, avatar, firstname } } = this.props
    return (
      <View style={{flex: 1}}>
        {currentRoom ?
        <GiftedChat
          renderLoading={() =>  <Preloader><ActivityIndicator size="large" color="#5AC6D1" /></Preloader>}
          isAnimated={true}
          loadEarlier={true}
          isLoadingEarlier={loadingRoom}
          messages={currentRoom.messages}
          onSend={(message) => this.props.sendMessage(currentRoom.room, message)}
          bottomOffset={Platform.OS === 'ios' ? 75 : null}
          user={{_id, avatar, firstname}}
        /> : null }
      </View>
    )
  }
}
const mapStateToProps = state => ({
  profile: state.user.profile,
  chat: state.chat,
  // blockedUser: state.match.blockedUser
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadRoom,
      leaveRoom,
      sendMessage
   },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: true })(Chat)


const Preloader = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
`