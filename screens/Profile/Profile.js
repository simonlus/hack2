import React from 'react'
import { StyleSheet, AsyncStorage, Dimensions, Image, View, Alert, StatusBar } from 'react-native'
import styled from 'styled-components/native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { update, editLocation, editBio, editAvatarUrl, profileLoaded } from '../../redux/modules/User'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Button from '../../components/Button'
import Activity from '../../components/Activity'
import Logout from '../../components/Logout'
import UploadImage from './UploadImage'

function capitalize (string) {
  return string[0].toUpperCase() + string.substring(1)
}

class Profile extends React.Component {
  // static navigationOptions = {
  //   title: 'Profile',
  //   headerLeft: null,
  //   gesturesEnabled: false,
  //   headerRight: <Logout />
  // }

  constructor(props){
    super(props);
    this.state = {
      isEditing: true,
      slider1ActiveSlide: 0,
      isOpen: false,
      showDots: true
    }
    this.heightDevice = Dimensions.get('window').height;
    this.widthDevice = Dimensions.get('window').width;
    this.imageHeight = Math.round(this.heightDevice * .65);
    this.imageWidth = Math.round(this.imageHeight / 2);

    this.close = this.close.bind(this);
    // this.edit = this.edit.bind(this);
    this.save = this.save.bind(this);
    this._renderItem = this._renderItem.bind(this)
  }

  async componentWillMount() {
    const firstTime = await AsyncStorage.getItem("firstTime");
    this.setState({isOpen: (firstTime !== "false") })
  }

  componentDidMount() {
    this.props.profileLoaded();
  }

  close() {
    AsyncStorage.setItem("firstTime", "false");
    this.setState({ isOpen: false });
  }

  // edit () {
  //   this.setState({
  //     isEditing: true
  //   })
  // }

  save () {
    // this.props.update({ ...this.state.user })
    // console.warn(this.props.user.bio.length);
    // if(this.props.user.bio.length < 80) {
    //   Alert.alert(
    //     null,
    //     'Bio should be at least 80 characters long')
    // } else {
    //   this.props.update(this.props.user)
    //   this.setState({
    //     isEditing: false
    //   })
    // }
    this.props.update(this.props.user)
  }

  _renderItem ({item, index}) {
    return (
        <View style={{alignItems: 'center', marginTop: 10}}>
            <Image source={item.image} style={{width: this.imageWidth, height: this.imageHeight, marginBottom: 10}} resizeMode="cover" />
            <CardText>{item.title}</CardText>
            <CardText>{item.description}</CardText>
            { index == 5 ?
            <Button
              text="Let's Fuckin' Go 🎉"
              inverted
              onPress={()=>{this.close()}} /> : null }
        </View>
    );
  }

  render () {
    const { navigation, update, user, editBio, editLocation, editAvatarUrl } = this.props
    let { isEditing } = this.state
    
    const styles = StyleSheet.create({
      activitiesShadow: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1
      }
    })

    const DATA = [
      { 
        image: require("../../assets/onboarding/1.png"), 
        title: "CURATE YOUR INFO", 
        description: "LOG IN VIA FACEBOOK AND DESCRIBE A LITTLE MORE ABOUT YOURSELF"
      },{ 
        image: require("../../assets/onboarding/2.png"), 
        title: "CHOOSE A CITY", 
        description: "CHOOSE A CITY YOU'RE IN OR PLANNING TO TRAVEL TO SOON"
      },{ 
        image: require("../../assets/onboarding/3.png"), 
        title: "SELECT YOUR INTERESTS", 
        description: "SELECT UP TO 3 CATEGORIES YOU'RE INTERESTED IN"
      },{ 
        image: require("../../assets/onboarding/4.png"), 
        title: "BROWSE OTHER PROFILES", 
        description: "BASED ON YOUR SELECTED LOCATION AND ACTIVITY INTERESTS"
      },{ 
        image: require("../../assets/onboarding/5.png"), 
        title: "FIND MATCHES", 
        description: "WHEN YOU AND ANOTHER USER SELECT EACH OTHER"
      },{ 
        image: require("../../assets/onboarding/6.png"), 
        title: "PLAN A SCAPADE!", 
        description: "GET TO KNOW EACH OTHER AND PLAN SOMETHING AWESOME"
      },
    ]

    if(this.state.isOpen) {
      return (
        <Onboarding visible={ this.state.isOpen } onRequestClose={()=>{this.close()}}>
          <StatusBar backgroundColor="#5AC6D1" barStyle="dark-content" />
          <OnboardingContainer>
            {/* <ButtonCircle onPress={()=>{this.close()}}><CircleImage source={require('../../assets/icons/markgrey.png')} /></ButtonCircle> */}
            <Carousel
              ref={c => this._slider1Ref = c}
              data={DATA}
              renderItem={this._renderItem}
              sliderWidth={this.widthDevice}
              itemWidth={this.widthDevice * .8}
              firstItem={0}
              inactiveSlideScale={0.90}
              inactiveSlideOpacity={0.7}
              inactiveSlideShift={50}
              initialNumToRender={3}
              maxToRenderPerBatch={3}
              // useScrollView={true}
              lockScrollTimeoutDuration={200}
              lockScrollWhileSnapping={true}
              loop={false}
              onBeforeSnapToItem={(index) => {
                if(index == 5 && this.state.showDots) {
                  this.setState({showDots: false});
                } else {
                  this.setState({showDots: true});
                }
                this.setState({ slider1ActiveSlide: index });
              }}
            />
            { this.state.showDots ? 
            <View style={{position: "absolute", bottom: 0, width: '100%'}}>
              <Pagination
                containerStyle={{}}
                dotsLength={DATA.length}
                activeDotIndex={this.state.slider1ActiveSlide}
                dotColor={'rgba(255, 255, 255, 0.82)'}
                inactiveDotColor={'rgba(0, 0, 0, 0.82)'}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={this._slider1Ref}
                tappableDots={!!this._slider1Ref}
              />
            </View> : null }
          </OnboardingContainer>
        </Onboarding>
      )
    }
    
    if (isEditing) {
      return (
        <KeyboardAwareScrollView
          extraScrollHeight={120}
          enableOnAndroid={true}
          keyboardShouldPersistTaps='handled'>
          <Container>
            <UploadImage avatar={user.avatar} editAvatarUrl={editAvatarUrl}/>
            <Content>
            <Information>
              <Name>{user.firstname}{user.age && ','} <Age>{user.age}</Age></Name>
              {user.facebookLocation &&
              <Location>
                <LocationIcon source={require('../../assets/location.png')} />
                <LocationText>{user.facebookLocation}</LocationText>
              </Location>
              }
            </Information>
              <Information>
                <Row>
                  <SelectCityRow>
                    <Title>LOCATION</Title>
                    <Button small text="Edit" onPress={() => navigation.navigate('SelectLocation', { 
                      editLocation: async (l) => {
                        await editLocation(l); 
                        this.save();
                      }})} />
                  </SelectCityRow>
                  <Title>{user.city ? user.city : '-'}</Title>
                </Row>
              </Information>
              <Row>
                <Title>TELL US ABOUT YOURSELF</Title>
                <Textarea
                  style={{textAlignVertical: "top"}}
                  onChangeText={async (t)=> {await editBio(t); this.save();}}
                  value={user.bio}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  multiline = {true}
                  numberOfLines = {2}
                  maxLength={180}
                  placeholder="Obsessed wth pizza. Any kind. Looking for a fellow foodie to explore the city with and find the best slice out there!" />
                  <Counter>{user.bio && user.bio.length}/80</Counter>
              </Row>
              <Row>
                <SelectCityRow>
                  <Title>YOUR INTERESTS</Title>
                  <Button small text="Edit" onPress={() => navigation.navigate('Activities')} />
                </SelectCityRow>
                <ActivitiesList>
                  {
                    user.activities &&
                    user.activities.map((activity, index) => (
                        <Activity disabled checked icon={activity} text={capitalize(activity)} key={index} />
                      ))
                  }
                  {/* <AddActivity onPress={() => navigation.navigate('Activities')}>
                    <AddActivityIconWrapper>
                      <AddActivityIcon source={require('../../assets/plus.png')} />
                    </AddActivityIconWrapper>
                  </AddActivity> */}
                </ActivitiesList>
              </Row>
              {/* <Button text="Save" onPress={() => this.save()} /> */}
              <Logout />
            </Content>
          </Container>
        </KeyboardAwareScrollView>
      )
    }

    // return (
    //   <Container>
    //     <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
    //     <PhotoWrapper>
    //       {/* {
    //         !isEditing &&
    //         <Edit onPress={() => this.edit()}>
    //             <EditIcon source={require('../../assets/settings.png')} />
    //           </Edit>
    //       } */}
    //       <Photo source={{ uri: user.avatar }} resizeMode="cover" />
    //     </PhotoWrapper>
    //     <Content>
    //       <Information>
    //         <Name>{user.firstname}{user.age && ','} <Age>{user.age}</Age></Name>
    //         {user.facebookLocation &&
    //         <Location>
    //           <LocationIcon source={require('../../assets/location.png')} />
    //           <LocationText>{user.facebookLocation}</LocationText>
    //         </Location>
    //         }
    //       </Information>
    //       {
    //         user.bio !== '' &&
    //         <Row>
    //             <Title>ABOUT YOURSELF</Title>
    //             <AboutYourself>{user.bio}</AboutYourself>
    //           </Row>
    //       }
    //       {
    //         user.activities &&
    //         <Row>
    //             <Title>YOUR INTERESTS</Title>
    //             <ActivitiesList>
    //               {
    //                 user.activities.map((activity, index) => (
    //                   <Activity disabled checked icon={activity} text={capitalize(activity)} key={index} />
    //                 ))
    //               }
    //             </ActivitiesList>
    //           </Row>
    //       }
    //       { user.city &&
    //       <Row>
    //         <Title>SELECTED LOCATION</Title>
    //         <SelectCityRow>
    //           <Title>{user.city ? user.city : '-'}</Title>
    //         </SelectCityRow>
    //       </Row>
    //       }
    //       <Button text="Fill out the profile" onPress={() => this.edit()} />
    //     </Content>
    //   </Container>
    // )
  }
}

const mapStateToProps = state => ({
  user: state.user.profile
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      update,
      editBio,
      editLocation,
      editAvatarUrl,
      profileLoaded,
    },
    dispatch
  )

export default connect(mapStateToProps, mapDispatchToProps)(Profile)

const Container = styled.ScrollView`
  flex: 1;
`

// const PhotoWrapper = styled.View`
//   position: relative;
//   align-items: center;
//   justify-content: center;
//   padding-top: 30px;
// `

// const ChangePhoto = styled.TouchableOpacity`
//   position: absolute;
//   top: 8px;
//   right: 8px;
//   z-index: 1;
//   background: #5AC6D1;
//   padding: 8px;
//   border-radius: 4px;
// `

// const ChangePhotoText = styled.Text`
//   color: #FFF;
//   font-size: 10px;
//   font-weight: 800;
// `

// const Edit = styled.TouchableOpacity`
//   position: absolute;
//   top: 24px;
//   right: 8px;
//   z-index: 1;
// `

// const EditIcon = styled.Image`
//   height: 24px;
//   width: 24px;
// `

// const Photo = styled.Image`
//   height: 150px;
//   width: 150px;
//   border-radius: 75px;
// `

const Content = styled.View`
  padding: 16px;
`

const Information = styled.View`
  /* margin-bottom: 32px; */
`

const Name = styled.Text`
  font-size: 22px;
`

const Age = styled.Text`
  font-size: 20px;
  font-weight: 800;
`

const Location = styled.View`
  flex-flow: row nowrap;
  align-items: center;
  margin-bottom: 32px;
`

const LocationText = styled.Text`
  font-size: 14px;
  margin-top: 4px;
  color: #a1a1a1;
`

const LocationIcon = styled.Image`
  margin-top: 4px;
  margin-right: 4px;
  height: 14px;
  width: 14px;
`

const Row = styled.View`
  margin-bottom: 16px;
`

const Title = styled.Text`
  font-size: 12px;
  font-weight: 600;
  color: #383838;
`

const ActivitiesList = styled.View`
  flex-flow: row wrap;
  margin: 24px 0 0 0;
`

// const AddActivity = styled.TouchableOpacity`
//   width: 33%;
//   align-items: center;
//   margin-bottom: 24px;
// `

// const AddActivityIconWrapper = styled.View`
//   height: 60px;
//   width: 60px;
//   background: #5AC6D1;
//   justify-content: center;
//   align-items: center;
//   border-radius: 30px;
//   margin-bottom: 8px;
// `

// const AddActivityIcon = styled.Image`
//   height: 25px;
//   width: 25px;
// `

const Textarea = styled.TextInput`
  margin-top: 8px;
  padding: 8px 16px;
  width: 100%;
  border-radius: 4px;
  borderWidth: 1px;
  borderColor: #5AC6D1;
  background: #fff;
`

// const Input = styled.TextInput`
//   margin-top: 8px;
//   padding: 8px 16px;
//   width: 100%;
//   border-radius: 4px;
//   borderWidth: 1px;
//   borderColor: #5AC6D1;
//   background: #fff;
// `
const SelectCityRow = styled.View`
  flex-flow: row nowrap;
  padding: 8px 0 0 0;
  justify-content: space-between;
`

// const AboutYourself = styled.Text`
//   font-size: 16px;
//   margin-top: 8px;
// `

const Onboarding = styled.Modal`
`;

const OnboardingContainer = styled.View`
  flex: 1;
  backgroundColor: #57c6d2;
  flexDirection: row;
  justifyContent: center;
  alignItems: center;
`;

const CardText = styled.Text`
  font-size: 16px;
  margin-bottom: 10px;
  text-align: center;
  color: white;
`;

// const ButtonCircle = styled.TouchableOpacity`
//   margin-right: 20px;
//   margin-top: 20px;
//   align-items: flex-end;
// `
// const CircleImage = styled.Image`
//   width: 40px;
//   height: 40px;
// `

const Counter = styled.Text`
  text-align: right;
  font-size: 12px;
  font-weight: 600;
  color: #383838;
`