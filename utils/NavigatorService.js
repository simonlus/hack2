import { NavigationActions, StackActions } from 'react-navigation'

export default {
  container: null,

  setContainer(container) {
    this.container = container
  },

  navigateAndReset(routeName, key = null, params) {
    var resetAction = StackActions.reset({
      index: 0, key, actions: [ NavigationActions.navigate({routeName, params})]
    });
    this.container.dispatch(resetAction);
  },

  navigate(routeName, params) {
    this.container.dispatch(
      NavigationActions.navigate({
        routeName,
        params
      })
    )
  },

  goBack(key) {
    this.container.dispatch(
      NavigationActions.back({
        key
      })
    )
  }
}
