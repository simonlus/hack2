import axios from 'axios'
// import NavigatorService from '../utils/NavigatorService'
import { StackActions, NavigationActions } from 'react-navigation'

// const BASE_URL = 'http://95.213.204.108'
export const BASE_URL = 'http://45.55.95.161'
// export const BASE_URL = 'http://192.168.15.11:3000'
// export const BASE_URL = 'http://192.168.1.72:3000'

axios.defaults.baseURL = BASE_URL
axios.defaults.headers.post['Content-Type'] = 'application/json'

const request = axios.create()

export const setupToken = token => {
  request.defaults.headers.common.Authorization = token
}

export const resetToken = _ => {
  request.defaults.headers.common.Authorization = undefined
}

request.interceptors.request.use(
  request => {
    return request
  },
  error => {
    // console.warn(error)
  }
)

request.interceptors.response.use(
  response => response,
  error => {
    if (error && error.response && error.response.status === 401) {
      // NavigationActions.navigate('InitialScreen')
      var resetAction = StackActions.reset({
        index: 0, key: null, actions: [ NavigationActions.navigate({routeName: 'InitialScreen'})]
      });
      navigation.dispatch(resetAction);
      // navigation.navigate('SearchCards');
    }
    // console.warn(error);
    return Promise.reject(error)
  }
)

export const post = (url, data, config = {}) => {
  // console.warn("post", url);
  return request.post(url, data, config);
}
export const get = (url, config = {}) => {
  // console.warn("get", url);
  return request.get(url, config);
}
export const put = (url, data, config = {}) => {
  return request.put(url, data, config);
}
export const del = (url, config = {}) => {
  // console.warn("del", url);
  return request.delete(url, config);
}
