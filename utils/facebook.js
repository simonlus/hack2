const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
  AccessToken
} = FBSDK;

const permissions = ['public_profile', 'email', 'user_location', 'user_birthday']

export async function login () {
  try {
    const loginResponse = await LoginManager.logInWithReadPermissions(permissions);
    if(loginResponse.isCancelled) {
      return {type: "cancel", token: null, msg: "Login was cancelled"}
    }
    
    const tokenResponse = await AccessToken.getCurrentAccessToken();
    const token = tokenResponse.accessToken.toString();
    return { type: 'success', token }
  } catch (err) {
    return {type: "error", token: null, msg: err}
  }
} // zLNyPnw8OLbA9bTQaLk9+5RS0Mc=