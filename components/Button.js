import React from 'react'
import styled, { css } from 'styled-components/native'
import { prop, ifProp, switchProp } from 'styled-tools'

export default class Button extends React.Component {
  render () {
    const { text, small, medium, micro, disabled, login, dropdown, onPress, inverted } = this.props

    return (
      <ButtonView
        size={micro ? 'micro' : small ? 'small' : medium ? 'medium' : 'default'}
        disabled={disabled}
        inverted={inverted}
        onPress={disabled ? null : onPress}
      >
        { login && <Icon source={require('../assets/icons/facebook.png')} resizeMode="contain" /> }
        <ButtonText
          inverted={inverted}
          size={micro ? 'micro' : small ? 'small' : medium ? 'medium' : 'default'}
          >
          {text}
        </ButtonText>
      </ButtonView>
    )
  }
}

const Icon = styled.Image `
  width: 25px;
  height: 25px;
  margin-right: 8px;
`

const ButtonView = styled.TouchableOpacity`
  ${switchProp('size', {
    micro: css`
    `,
    small: css`
    `,
    medium: css`
    `,
    default: css`
      flex-direction: row;
      align-items: center;
      justify-content: center;
    `,
  })};
  width: ${switchProp('size', {
    micro: 'auto',
    small: 'auto',
    medium: '100%',
    default: '100%'
  })};
  padding: ${switchProp('size', {
     micro: '5px',
     small: '0px',
     medium: '8px',
     default: '16px'
   })};
  background-color: ${switchProp('size', {
     micro: 'transparent',
     small: 'transparent',
     medium: '#5AC6D1',
     default: '#5AC6D1'
   })};

  ${ifProp('disabled', 'background-color: #CCC;', '')}
  ${ifProp('inverted', 'background-color: #FFF; border-color: #5AC6D1', '')}

  ${ifProp({size: 'micro'}, 'padding-right: 20; padding-left: 20; margin-top: 10;','')}
  border-radius:  ${switchProp('size', {
    micro: '20px',
    small: '20px',
    medium: '20px',
    default: '30px'
  })};
`

const ButtonText = styled.Text`
  text-align: ${ifProp({size: 'small'}, 'center', 'center')};
  color: ${ifProp({size: 'small'}, '#5AC6D1', '#FFF')};
  ${ifProp('inverted', 'color: #5AC6D1;', '')}
  ${switchProp('size', {
    small: css`
    `,
    medium: css`
      font-size: 14px;
      font-weight: 400;
    `,
    default: css`
      font-size: 18px;
      font-weight: 600;
    `,
   })};
`
