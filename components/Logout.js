import React from 'react'
import styled from 'styled-components/native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { logout } from '../redux/modules/User'
import Button from './Button';

const Logout = ({ logout }) => (
  <Button onPress={logout} text="Logout" />
)

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logout
    },
    dispatch
  )

export default connect(null, mapDispatchToProps)(Logout)