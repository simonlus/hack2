import React from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import styled from 'styled-components/native'

import SmallActivity from './SmallActivity'

export default class Card extends React.PureComponent {
  constructor(props){
    super(props);
    this._handleAccept = this._handleAccept.bind(this);
    this._handleDecline = this._handleDecline.bind(this);
  }

  _handleAccept(){
    this.props.onAccept && this.props.onAccept(this.props.data);
  }
  _handleDecline(){
    this.props.onDecline && this.props.onDecline(this.props.data);
  }
  render () {
    const { data } = this.props

    const styles = StyleSheet.create({
      card: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 2
      }
    })
    
    return (
      <View style={{padding: 10}}>
        <CardView style={styles.card}>
          <PhotoWrapper style={styles.photo}>
            <Photo source={{ uri: data.avatar, cache: 'force-cache' }} />
          </PhotoWrapper>
          <Information>
            <Name>{data.firstname}, <Age>{data.age}</Age></Name>
            { data.facebookLocation == 'New York' ? null : 
              <Location>
                  <LocationIcon source={require('../assets/location.png')} />
                  <LocationText>{data.facebookLocation}</LocationText>
              </Location>
            }
            <Description>{data.bio}</Description>
            <Activities>
              {
                data.activities.map((activity, index)=> (
                  <SmallActivity key={index} icon={activity} />
                ))
              }
            </Activities>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 5, marginTop: 15}}>
              <ButtonCircle onPress={this._handleDecline}><CircleImage source={require('../assets/icons/markblue.png')} /></ButtonCircle>
              <ButtonCircle onPress={this._handleAccept}><CircleImage source={require('../assets/icons/tick.png')} /></ButtonCircle>
            </View>
          </Information>
        </CardView>
      </View>
    )
  }
}

const CardView = styled.View`
  border-radius: 6px;
  align-items: center;
  justify-content: center;
  background: #FFFFFF;
`
const height = Math.min(Dimensions.get('window').height * .30, 300);
//const width = Dimensions.get('window').width * .8;

const PhotoWrapper = styled.View`
  margin-top: 16px;
  width: ${height}px;
  height: ${height}px;
  border-radius: ${height/2}px;
  overflow: hidden;
`
// console.warn(height);
const Photo = styled.Image`
  overflow: hidden;
  height: 100%;
  width: 100%;
`

const Information = styled.View`
  width: 100%;
  padding: 16px 16px;
`

const Name = styled.Text`
  font-size: 22px;
  text-align: center;
`

const Age = styled.Text`
  font-size: 20px;
  font-weight: 800;
`

const Location = styled.View`
  flex-flow: row nowrap;
  align-items: center;
  justify-content: center;
`

const LocationText = styled.Text`
  font-size: 14px;
  margin-top: 4px;
  color: #a1a1a1;
`

const LocationIcon = styled.Image`
  margin-top: 4px;
  margin-right: 4px;
  height: 14px;
  width: 14px;
`

const Description = styled.Text`
  margin-top: 8px;
  color: #4A4A4A;
  text-align: center;
`

const Activities = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: center;
`

const ButtonCircle = styled.TouchableOpacity`
  padding: 5px;
  margin-left: 10px;
`
const CircleImage = styled.Image`
  width: 50px;
  height: 50px;
`

const Activity = styled.Text`

`
