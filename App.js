import React from 'react'
import styled from 'styled-components/native'
import { ActivityIndicator, View } from 'react-native'
import { createBottomTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import OneSignal from 'react-native-onesignal'; // Import package from node modules

// import { updateFocus, getCurrentRouteKey } from '@patwoz/react-navigation-is-focused-hoc'
import { Provider } from 'react-redux'
import configureStore from './redux/store'
import NavigatorService from './utils/NavigatorService'

import {
  setCustomText
} from 'react-native-global-props';
import InitialScreen from './screens/Login/InitialScreen'
import Terms from './screens/Login/Terms'

import Profile from './screens/Profile/Profile'
import Activities from './screens/Profile/Activities'
import SelectLocation from './screens/Profile/SelectLocation'

import Gather from './screens/Gather/Gather'
import Explore from './screens/Explore/Explore'

import SearchLoader from './screens/Search/Loader'
import SearchCards from './screens/Search/Cards'
import SearchStart from './screens/Search/Start'
import SearchLocation from './screens/Search/Location'
import SearchActivities from './screens/Search/Activities'

import Messages from './screens/Chat/Messages'
import Chat from './screens/Chat/Chat'

import TabBarTop from './components/TabBarTop'

import { loadSession } from './redux/modules/User'
import { messagePushClicked } from './redux/modules/Chat/ChatActions'
import { updateToken } from './redux/modules/Notification/NotificationActions'

const LoginNavigation = createStackNavigator({
  InitialScreen: { screen: InitialScreen },
  Terms: { screen: Terms }
}, {
  mode: 'modal',
  headerMode: 'screen',
  swipeEnabled: false
})

const ProfileNavigation = createStackNavigator({
  Profile: { screen: Profile },
  Activities: { screen: Activities },
  SelectLocation: { screen: SelectLocation }
}, {
  headerMode: 'none',
  swipeEnabled: false,
  cardStyle: { flex: 1, backgroundColor: "#FFF" }
})

const ExploreNavigation = createStackNavigator({
  Explore: { screen: Explore }
}, {
  headerMode: 'none',
  swipeEnabled: false,
  cardStyle: { flex: 1, backgroundColor: "#5AC6D1" }
})

const GatherNavigation = createStackNavigator({
  Gather: { screen: Gather }
}, {
  headerMode: 'none',
  swipeEnabled: false,
  cardStyle: { flex: 1, backgroundColor: "#5AC6D1" }
})

const SearchNavigation = createStackNavigator({
  // SearchStart: { screen: SearchStart },
  // SearchLoader: { screen: SearchLoader },
  // SearchActivities: { screen: SearchActivities },
  // SearchLocation: { screen: SearchLocation },
  SearchCards: { screen: SearchCards }
}, {
  headerMode: 'none',
  swipeEnabled: false,
  cardStyle: { flex: 1, backgroundColor: "#5AC6D1" }
})

const ChatNavigation = createStackNavigator({
  Messages: { screen: Messages },
  Chat: { screen: Chat },
}, {
  headerMode: 'float',
  swipeEnabled: false,
  cardStyle: { flex: 1, backgroundColor: "#FFF" }
})

ChatNavigation.navigationOptions = ({ navigation }) => {
  if(navigation.state && navigation.state.routes) {
    const tabBarVisibleArr =  navigation.state.routes.map(r => r.params && r.params.tabBarVisible);
    const tabBarVisible = !tabBarVisibleArr.filter(v => v === false).length > 0;
    return { tabBarVisible }
  }
};

const store = configureStore()

const TabNavigation = createBottomTabNavigator({
  ProfileTab: { screen: ProfileNavigation },
  ExploreTab: { screen: ExploreNavigation },
  SearchTab: { screen: SearchNavigation },
  GatherTab: { screen: GatherNavigation },
  ChatTab: { screen: ChatNavigation }
}, {
  tabBarPosition: 'bottom',
  tabBarComponent: TabBarTop,
  swipeEnabled: false,
  animationEnabled: false
})

const MainNavigator = createStackNavigator({
  LoginTab: { screen: LoginNavigation },
  PrivateTab: { screen: TabNavigation },
}, {
  headerMode: 'none',
  swipeEnabled: false,
});
const NavApp = createAppContainer(MainNavigator);

export default class App extends React.Component {
  state = {
    fontLoaded: false,
    isReady: false,
  };

  constructor(props) {
    super(props);
    OneSignal.init("db925fdf-6a0f-4d98-ae1b-b5ebda860f64");
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(2);
    OneSignal.configure();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    // console.warn("Notification received: ", notification);
  }

  onOpened(openResult) {
    const { additionalData } = openResult.notification.payload;
    // console.warn('Message: ', openResult.notification.payload.body);
    // console.warn('Data: ', openResult.notification.payload.additionalData);
    // console.warn('isActive: ', openResult.notification.isAppInFocus);
    // console.warn('openResult: ', openResult);
    if(additionalData) {
      // console.warn(additionalData);
      if(additionalData.type == "message") {
        store.dispatch(messagePushClicked(additionalData));
      }
    }
  }

  onIds(device) {
    if(device && device.pushToken) {
      store.dispatch(updateToken(device.pushToken, device.userId))
    }
    // console.warn(device);
  }

  async componentDidMount () {
    this.setState({isReady: true});
    const customTextProps = {
      style: {
        fontFamily: 'Proxima Nova'
      }
    };
    setCustomText(customTextProps);
    store.dispatch(loadSession());
  }

  render () {
    if (!this.state.isReady) {
      return (
        <View style={{flex: 1, justifyContent: 'center', backgroundColor: '#FFF'}}>
          <ActivityIndicator size="large" color="#5AC6D1" />
        </View>
      );
    }

    return (
      <Provider store={store}>
        {
          <NavApp ref={navigatorRef => {
            this.navigatorRef = navigatorRef;
            NavigatorService.setContainer(navigatorRef);
          }} />
        }
      </Provider>
    )
  }
}

