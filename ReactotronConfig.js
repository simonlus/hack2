import Reactotron from 'reactotron-react-native'
// import { reactotronRedux as reduxPlugin } from 'reactotron-redux'
// import sagaPlugin from 'reactotron-redux-saga'

if (__DEV__) {
    Reactotron
        .configure() // controls connection & communication settings
        .useReactNative() // add all built-in react native plugins
        // .use(reduxPlugin())
        // .use(sagaPlugin())
        .connect() // let's connect!
}