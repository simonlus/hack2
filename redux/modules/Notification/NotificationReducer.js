import { handleActions } from 'redux-actions';

const initialState =     {
    token: null,
}

export default handleActions({
    UPDATE_TOKEN: (state, action) => ({
        ...state,
        updatingToken: true,
    }),
    UPDATE_TOKEN_SUCCESS: (state, action) => ({
        ...state,
        updatingToken: false,
    }),
    UPDATE_TOKEN_ERROR: (state, action) => ({
        ...state,
        updatingToken: false,
    }),
}, initialState );