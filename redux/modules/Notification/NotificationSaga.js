import { all, take, takeLatest, call, put } from 'redux-saga/effects'
import {Platform} from 'react-native';
import { put as putRequest } from '../../../utils/requests'
import { LOAD_SESSION, loginSuccess } from '../User/UserReducer'
import Permissions from 'react-native-permissions'

function* updateToken(action) {
    try {
        yield take([LOAD_SESSION, loginSuccess]) // Wait for login or session load, then update token
        const { token, userId } = action.payload;
        const { data } = yield call(putRequest, '/api/protected/users/push', { token, userId })
        if(data.success){
            yield put({type: 'UPDATE_TOKEN_SUCCESS', data: data.data })
        } else {
            yield put({type: 'UPDATE_TOKEN_ERROR' })
        }
    } catch (error) {
        yield put({type: 'UPDATE_TOKEN_ERROR', error})
    }
}

async function* prepareNotifications() {
    if(Platform.OS === 'ios') {
        const hasPermissions = await Permissions.check('notification')
        if(hasPermissions != 'authorized') {
            // console.warn(hasPermissions);
            const response = await Permissions.request('notification');
            if(response == 'authorized') { // 'authorized', 'denied', 'restricted', or 'undetermined'
                //
            } 
        }
    }
}

const notificationSaga = function* () {
    yield all([
        takeLatest('UPDATE_TOKEN', updateToken),
        prepareNotifications()
    ])
}
  
export default notificationSaga