import { createActions } from 'redux-actions';

const actions = createActions({
    UPDATE_TOKEN: (token, userId) => ({token, userId}),
});

export const updateToken = actions.updateToken;