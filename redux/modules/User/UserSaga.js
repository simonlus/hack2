import { delay } from 'redux-saga'
import { all, fork, takeLatest, call, put, select } from 'redux-saga/effects'
import { UPDATE, LOGIN, LOGOUT, LOAD_SESSION, loginSuccess, logout, loginFail, loadSessionFail, updateSuccess } from './UserReducer'
import { fetchMatches } from '../Match'
// import { sendNotification } from '../System'

import { login } from '../../../utils/facebook'
import { post, put as putRequest, setupToken, resetToken } from '../../../utils/requests'
import AsyncPhoneStorage from '../../../utils/AsyncPhoneStorage'
import NavigatorService from '../../../utils/NavigatorService'
import SplashScreen from 'react-native-splash-screen';
// import { StackActions, NavigationActions } from 'react-navigation'

// ENDPOINTS

const LOGIN_ENDPOINT = '/api/users'
const UPDATE_PROFILE_ENDPOINT = (userId) => `/api/protected/users/${userId}`

// SAGAS

const hideSplashScreen = function*(){
  // yield delay(200)
  yield call(SplashScreen.hide)
}

const navigateToSection = function*() {
  yield call([NavigatorService, NavigatorService.navigateAndReset], 'PrivateTab')
  // NavigatorService.navigateAndReset('PrivateTab') 
  const firstSearchDone = yield call([AsyncPhoneStorage, AsyncPhoneStorage.getItem], '@UserStore:firstSearch')
  if(firstSearchDone == "true"){
    yield call([NavigatorService, NavigatorService.navigate], 'SearchTab')
  }
}

const loadSessionSaga = function*() {
  try {
    const token = yield AsyncPhoneStorage.getItem('@UserStore:token')
    // ok, here its can be null

    if (typeof token === 'string') {
      const { data }  = yield post(LOGIN_ENDPOINT, { token })
      if(data.success) {
        setupToken(data.data.token)
        yield put(loginSuccess(data.data))
        // additional requests initial
        yield put(fetchMatches())
        // yield put(sendNotification())
        // yield put(startChannel())
        yield call(navigateToSection)
      } else {
        yield put(logout());
        yield put(loadSessionFail("FB Token expired"));
      }
    }
    yield call(hideSplashScreen)
  } catch (error) {
    yield put(loadSessionFail(error))
    yield call(hideSplashScreen)
    // console.error(error)
  }
}

const loginSaga = function*() {
  try {
    // call facebook login
    const { type, token, msg } = yield call(login)
    
    if (type === 'success') {
      yield AsyncPhoneStorage.setItem('@UserStore:token', token)
      const { data } = yield post(LOGIN_ENDPOINT, { token })
      if(data.success) {
        setupToken(data.data.token)
        yield put(loginSuccess(data.data))
        // yield put(startChannel())
        yield call(navigateToSection)
      } else {
        yield put(loginFail("Failed to authenticate. Error: " + data.data));
      } 
    } else if (type === 'cancel') {
      yield put(loginFail("Failed to authenticate with Facebook, reason: User canceled login."));
    } else {
      yield put(loginFail("Failed to authenticate with Facebook. Error: " + msg + "["+ type +"]"));
    }
    yield call(hideSplashScreen)
  } catch (error) {
    yield call(hideSplashScreen)
    yield put(loginFail("Failed to authenticate with Facebook! Error: " + error.toString()));
  }
}

const updateProfileSaga = function*(action) {
  try {
    const userId = yield select(state => state.user.profile['_id'])
    const { data } = yield putRequest(UPDATE_PROFILE_ENDPOINT(userId), { ...action.payload })
    yield put(updateSuccess(data.data))
  } catch (error) {
    // console.error(error)
  }
}

const logoutSaga = function*() {
  yield AsyncPhoneStorage.removeItem('@UserStore:token')
  resetToken()
  // yield put(startChannel())
  NavigatorService.navigateAndReset('LoginTab')
}

const userSaga = function*() {
  yield all([
    takeLatest(LOGIN, loginSaga),
    takeLatest(LOGOUT, logoutSaga),
    takeLatest(LOAD_SESSION, loadSessionSaga),
    takeLatest(UPDATE, updateProfileSaga)
  ])
}

export default userSaga
