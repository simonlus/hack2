import { all, take, takeFirst, takeLatest, takeEvery, call, fork, put, cancelled, select } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import { get, post, setupToken } from '../../../utils/requests'
// import { Permissions, Notifications } from 'expo'
// import { Platform, Alert } from 'react-native'
// import { sendNotification } from '../System';
import NavigatorService from '../../../utils/NavigatorService'

function createDsChannel(client, eventName) {
    return eventChannel(emit => {
        setImmediate(() => {
            console.debug(`subscribing to channel: ${eventName}`);
            client.event.unsubscribe(eventName);
            client.event.subscribe(eventName, emit);
        });
        const unsubs = () => {
            console.debug(`unsubscribing from channel: ${eventName}`);
            client.event.unsubscribe(eventName, emit);
        }
        return unsubs;
    }); 
}

function* subscribeToDsEvent(client, eventName, onRecordChange) {
    let channel;
    try {
      channel = yield call(createDsChannel, client, eventName);
    } catch (chanErr) {
    //   console.error(`Error creating subscription channel for deepstream item ${eventName}`, chanErr);
      throw chaneErr;
    }

    while (true) {
      try {
        const item = yield take(channel);
        //console.debug(`Got an event named '${eventName}'`, item);
        yield fork(onRecordChange, item);
      } catch (err) {
        if (yield cancelled()) {
          channel.close();
          return;
        } else {
        //   console.error(`Error in saga task for deepstream subscription of '${eventName}'`, err);
        }
      }
    }
}

function sendMessage({client}){
    return function* ({payload}) {
        yield call([client.event, client.event.emit], `message/${payload.room}`, payload);
    }
}

function* getRoom({payload}){
    try {
        const { data } = yield get(`api/protected/chats/${payload.room}`)
        if(data.success) {
            // We need to populate user for each message,
            // done here from members to reduce network transfer
            const chat = {
                ...data.data,
                messages: data.data.messages.map((msg)=>{
                    return {
                        ...msg, 
                        user: data.data.members.filter(mem=>mem.user.id == msg.user)[0].user
                    }
                })
            }
            yield put({type: 'LOAD_ROOM_SUCCESS', data: chat })
        } else {
            yield put({type: 'LOAD_ROOM_ERROR'})
        }
    } catch (error) {
        yield put({type: 'LOAD_ROOM_ERROR', error})
    }
}

function* sendNewMessagePush(icon, title, body, data) {
    // yield call(Notifications.presentLocalNotificationAsync, {
    //     title, body, data, android: { channelId: 'chat-messages', icon },
    // });
}

function listenForMessages({client}) {
    return function* (room) {
        yield fork(subscribeToDsEvent, client, `message/${room}`, function*(record) {
            if(!record.message || !record.message.length || !record.room) return;
            const { user: { _id, avatar, firstname }, text} = record.message[0]
            const { profile } = yield select(state => state.user)
            const { currentRoom } = yield select(state => state.chat)
            if(_id != profile._id && !currentRoom || currentRoom.room != room) // If not us, and not in chat display Push
                yield fork(sendNewMessagePush, avatar, firstname, text, {room})
            yield put({type: 'GOT_MESSAGE', data: record })
        })
    }
}

function newMatch({client}) {
    return function* ({payload: {chat}}) {
        if(chat) {
            // console.warn(chat);
            const task = yield fork(listenForMessages({client}), chat.room)
        }
    }
}

function* getConversations({client}) {
    try {
        const { data } = yield call(get,'api/protected/chats')
        if(data.success) {
            for(var i = 0; i < data.data.length; i++) {
                const task = yield fork(listenForMessages({client}), data.data[i].room)
            }
            yield put({type: 'LOAD_CONVERSATIONS_SUCCESS', data: data.data })
        } else {
            yield put({type: 'LOAD_CONVERSATIONS_ERROR'})
        }
    } catch (error) {
        yield put({type: 'LOAD_CONVERSATIONS_ERROR', error})
    }
}

let profileLoaded = false;

function* goToRoom({payload: {data}}) {
    if(!profileLoaded) {
        yield take('user/PROFILE_LOADED')
        yield put({type: 'LOAD_CONVERSATIONS'})
        yield take('LOAD_CONVERSATIONS_SUCCESS')
        profileLoaded = true;
    }
    NavigatorService.navigate('Chat', { room: data.room });
}

function* waitForProfileLoad() {
    yield take('user/PROFILE_LOADED')
    profileLoaded = true;
}

const chatSagas = function* ({client}) {
    yield all([
        takeLatest('LOAD_CONVERSATIONS', getConversations, {client}),
        takeLatest('LOAD_ROOM', getRoom),
        takeEvery('SEND_MESSAGE', sendMessage({client})),
        takeEvery('match/MATCH_ACCEPT_SUCCESS', newMatch({client})),
        takeEvery('MESSAGE_PUSH_CLICKED', goToRoom),
        waitForProfileLoad()
    ])
}
  
export default chatSagas