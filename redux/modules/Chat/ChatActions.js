import { createActions } from 'redux-actions';

const actions = createActions({
    LOAD_CONVERSATIONS: () => ({}),
    LOAD_ROOM: (room) => ({room}),
    SEND_MESSAGE: (room, message) => ({room, message}),
    LEAVE_ROOM: (room) => ({room}),
    MESSAGE_PUSH_CLICKED: (data) => ({data})
});

export const loadConversations = actions.loadConversations;
export const loadRoom = actions.loadRoom;
export const leaveRoom = actions.leaveRoom;
export const sendMessage = actions.sendMessage;
export const messagePushClicked = actions.messagePushClicked;