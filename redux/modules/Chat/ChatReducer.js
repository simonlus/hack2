import { handleActions } from 'redux-actions';

const initialState =     {
    conversations: [],
    currentRoom: null,
    loading: false,
    loadingRoom: true,
    error: null
}

export default handleActions({
    LOAD_CONVERSATIONS_SUCCESS: (state, action) => ({
        ...state,
        loading: false,
        conversations: action.data,
    }),
    LOAD_CONVERSATIONS_ERROR: (state, action) => {
        const error = (action.error && action.error.response) ? action.error.response.data.err : null;
        return { 
            ...state, 
            loading: false,
            error 
        }
    },
    LOAD_CONVERSATIONS: (state, action) => ({
        ...state,
        error: null,
        currentRoom: null,
        loading: true
    }),
    LOAD_ROOM_SUCCESS: (state, action) => {
        const { conversations } = state
        const { room } = action.data
        let thisConv = conversations.find(c=>c.room == room)
        thisConv.unread = 0;
        return {
            ...state,
            loadingRoom: false,
            currentRoom: action.data,
            conversations
        }
    },
    LOAD_ROOM_ERROR: (state, action) => {
        const error = (action.error && action.error.response) ? action.error.response.data.err : null;
        return { 
            ...state,
            loadingRoom: false,
            currentRoom: null, 
            error 
        }
    },
    LOAD_ROOM: (state, action) => ({
        ...state,
        error: null,
        currentRoom: null,
        loadingRoom: true
    }),
    LEAVE_ROOM: (state, action) => ({
        ...state,
        currentRoom: null
    }),
    GOT_MESSAGE: (state, action) => {
        const { currentRoom, conversations } = state
        const { message, room } = action.data

        let thisConv = conversations.find(c=>c.room == room)
        // console.warn(thisConv);
        if(!currentRoom || currentRoom.room != thisConv.room) {
            thisConv.unread++;
        }
        thisConv.lastMessage = message[0]

        return {
            ...state,
            conversations: [thisConv, ...conversations.filter(c=>c!=thisConv)], // update conversations, move this one to top and update last message
            currentRoom: currentRoom && currentRoom.room == thisConv.room ? { // If we are in the room, also concat received message
                ...currentRoom,
                messages : message.concat(currentRoom.messages)
            } : currentRoom,
        }
    },
    'match/MATCH_ACCEPT_SUCCESS': (state, action) => {
        const { mutual, chat, user } = action.payload
        if(mutual && chat){
            const {id, room} = chat
            const newConv = { id: id, user, lastMessage: "", room: room, sortDate: null }
            return {
                ...state,
                conversations: [newConv, ...state.conversations]
            }
        } else {
            return state;
        }
    },
    // 'match/MATCH_REPORT_SUCCESS': (state, action) => ({
    //     ...state,
    //     conversations: state.conversations.filter(c=>c.room.indexOf(action.payload.user._id) == -1)
    // }),
    'match/MATCH_BLOCK_SUCCESS': (state, action) => ({
        ...state,
        conversations: state.conversations.filter(c=>c.room.indexOf(action.payload.user._id) == -1)
    }),
}, initialState );