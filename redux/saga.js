import { all } from 'redux-saga/effects'
import { userSaga } from './modules/User'
import { matchSaga } from './modules/Match'
// import { startStopChannel } from './modules/Chat'
// import { systemSaga } from './modules/System'
import chatSaga from './modules/Chat/ChatSaga'
import notificationSaga from './modules/Notification/NotificationSaga'

const rootSaga = function* rootSaga({ client }) {
  yield all([
    userSaga(),
    matchSaga(),
    // startStopChannel(),
    // systemSaga(),
    chatSaga({ client }),
    notificationSaga()
  ])
}
export default rootSaga
